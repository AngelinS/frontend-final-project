import { Component } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-follow',
  templateUrl: './follow.component.html',
  styleUrls: ['./follow.component.css']
})
export class FollowComponent {
  // constructor(private userService: UserService) {}

  // followUser(userId: number) {
  //   this.userService.followUser(userId).subscribe(() => {
  //     console.log('User followed successfully');
  //   });
  // }

  // unfollowUser(userId: number) {
  //   this.userService.unfollowUser(userId).subscribe(() => {
  //     console.log('User unfollowed successfully');
  //   });
  // }
}
