import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { FollowingComponent } from './following/following.component';


import { AllpostComponent } from './allpost/allpost.component';
import { UserComponent } from './user/user.component';
import { FollowersComponent } from './followers/followers.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { PostComponent } from './post/post.component';
import { UserListComponent } from './user-list/user-list.component';
// import { UserDetailsComponent } from './userdetails/userdetails.component';
// import { UserListComponent } from './user-list/user-list.component';
const routes: Routes = [{
  path: 'login',
  component: LoginComponent},
  // {path:'',redirectTo:'/login',pathMatch:'full'},
  
  { path:'profile',component:ProfileComponent},



 
{
path: 'home',
component: HomeComponent,

},
{

path: 'register',
component: RegisterComponent,
},
{ path: 'followers', component: FollowersComponent },
{ path: 'following', component: FollowingComponent },


{path:'allpost', component:AllpostComponent},
{path:'user', component:UserComponent},

{ path: 'create-post', component: CreatePostComponent },
{ path: 'post', component:PostComponent },

{ path: '', redirectTo: '/post', pathMatch: 'full' },

{ path: 'UserList', component: UserListComponent },
// { path: 'user-details/:id', component: UserDetailsComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


