import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private apiUrl = 'http://localhost:8080';

  constructor(private httpClient: HttpClient) { }

  uploadImage(reqParam: FormData) {
    return this.httpClient.post(this.apiUrl + '/img/fileSys', reqParam, { observe: 'response' });

  }

  getImage(id: number): Observable<Blob> {
    return this.httpClient.get(`${this.apiUrl}/img/fileSys/${id}`, { responseType: 'blob' });
  }

  getUploadImage(name: string) {
   return this.httpClient.get(this.apiUrl+'/img/get/img/info/'+name);
  }
  getone(){
    return this.httpClient.get(this.apiUrl+'/image//get/img/info/');
  }
 
}
