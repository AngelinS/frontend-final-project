

// import { Component, OnInit } from '@angular/core';
// import { ActivatedRoute } from '@angular/router';
// import { UserService } from '../user.service';
// import { User } from '../user.model';

// @Component({
//   selector: 'app-user-details',
//   templateUrl: './user-details.component.html',
//   styleUrls: ['./user-details.component.css']
// })
// export class UserDetailsComponent  {
//   user!: User;
//   userId!: number;

//   constructor(private route: ActivatedRoute, private userService: UserService) {}

//   ngOnInit() {
//     // Get the user ID from the route parameters
//     this.route.params.subscribe((params) => {
//       this.userId = +params['id']; // Convert to a number

//       // Fetch user details using the UserService
//       // Fetch user details using the UserService
//       this.userService['getUserById'](this.userId).subscribe(
//         (user: User) => {
//           this.user = user;
//         },
//         (error: any) => {
//           console.error('Error fetching user details:', error);
//           // Handle error, e.g., display an error message to the user
//         }
//       );
//     });
//   }
// }
