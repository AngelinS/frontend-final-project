import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ProfileService } from '../profile.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {

  uploadImage!: File;
  imageUrl: any;
  imagefile: any
  postResponse: any;
  dbImage: any;
  
urllink:string="insert/images/img3.jpg";
  post: any;
constructor(private httpClient: HttpClient, private profileService:ProfileService) {}

selectFiles(event:any){
  if(event.target.files){
    var reader=new FileReader()
    reader.readAsDataURL(event.target.files[0])
    reader.onload=(event:any)=>{
      this.urllink=event.target.result



      this.httpClient
          .post('http://localhost:8080/upload-image', { imageData: event.target.result })
          .subscribe((response) => {
            console.log('Image uploaded successfully.');
          });
      };
    }
  }



  uploadImageToFile(event: any) {
    this.uploadImage = event.target.files[0];
  }

  getImage() {
    const name = this.uploadImage.name;
    this.profileService.getUploadImage(name).subscribe((data:any)=>{
      this.postResponse = data;
        this.dbImage = 'data:image/jpeg;base64,' + this.postResponse.filePath;
    })
  }

  save() {
    let formData = new FormData();
    formData.append('image', this.uploadImage, this.uploadImage.name);
    this.profileService.uploadImage(formData).subscribe((res: any) => {
      if (res) {
        console.log(res);
        this.getImage();
      }
    })
  }




  getImages() {
    this.profileService.getone().subscribe(
      (val) => {
       this.post=val;
      
        console.log(this.post)
        this.post.forEach((element: { filePath: any; }) => {
          element.filePath 
          element.filePath = 'data:image/jpeg;base64,'+element.filePath;
        });
        
      }
    );
  }
}

  
  
  


 








